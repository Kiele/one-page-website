const arrDown = document.querySelector('.icon-arrdown');
const menuBtn = document.querySelector('.open-menu-btn');
const navigation = document.querySelector('.page-navigation');
const scrollTop = document.querySelector('.scroll-top');
const winHeight = window.innerHeight;

arrDown.addEventListener('click',()=>{
    window.scrollTo(0,winHeight);
});

scrollTop.addEventListener('click',()=>{
    window.scrollTo(0,0);
});

let click = 0;
menuBtn.addEventListener('click',()=>{
    !click ? click++ : click--
    navigation.classList.toggle('active');
    menuBtn.classList.toggle('active');
});

window.addEventListener('scroll',()=>{
    let scroll = window.scrollY;
    if(scroll>winHeight*1.5)
        scrollTop.classList.add('active');
    else if(winHeight<scroll)
        scrollTop.classList.remove('active');
});

(function(){
    let counter = 0;
    const sliderDots = document.querySelector('.slider-dots');
    const news = document.querySelectorAll('.news .entry');
    const init = function(){
        const prevButton = document.createElement('button');
        const nextButton = document.createElement('button');
        prevButton.classList.add('prev-news','fas','fa-chevron-left');
        nextButton.classList.add('next-news','fas','fa-chevron-right');
        sliderDots.prepend(prevButton);
        news.forEach((item,i) =>{
            const li = document.createElement('li');
            sliderDots.appendChild(li);
            if(!i){
                li.classList.add('active');
                item.classList.add('active');
            }
        });
        sliderDots.appendChild(nextButton);
        prevButton.addEventListener('click',prevSlide);
        nextButton.addEventListener('click',nextSlide);
        setInterval(()=>{
            nextSlide();
        },10000);
    }
    const nextSlide = function(){
        counter<news.length-1 ? counter++ : counter = 0;
        changeSlide(counter);
    }
    const prevSlide = function(){
        counter ? counter-- : counter = 2;
        changeSlide(counter);
    }
    const changeSlide = counter =>{
        const dots = document.querySelectorAll('.slider-dots li');
        dots.forEach(item=>{
            item.classList.remove('active');
        });
        news.forEach(item=>{
            item.classList.remove('active');
        });
        dots[counter].classList.add('active');
        news[counter].classList.add('active');
    }
    init();
}());
